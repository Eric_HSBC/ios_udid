/**
 * Created by Wayne
 * vps Model
 * devices模型
 */


const  mongoose = require('mongoose'),
      shortid = require('shortid'),
       Schema = mongoose.Schema;

let devicesSchema = new Schema({
  _id: {
    type: String,
    'default': shortid.generate
  },
  name:String,
  udid:String,
  register_time:{
    type: Date,
    "default": Date.now
  }
});

let devices = mongoose.model('devices', devicesSchema);

module.exports = devices;
