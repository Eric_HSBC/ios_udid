/**
 * Created by Wayne
 * vps Model
 * exportflag模型
 */


const  mongoose = require('mongoose'),
      shortid = require('shortid'),
       Schema = mongoose.Schema;

let exportflagSchema = new Schema({
  _id: {
    type: String,
    'default': shortid.generate
  },
  name:String,
  flag:String,
  register_time:{
    type: Date,
    "default": Date.now
  }
});

let exportflag = mongoose.model('exportflag', exportflagSchema);

module.exports = exportflag;
